const express = require('express')
const app = express()

const port = 5000

const helmet = require('helmet')
app.use(helmet())

app.use(express.static('public'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const router = require('./router_module')
const userRouter = require('./userRouter_module')

app.use('/', router)
app.use('/user', userRouter)

app.listen(port)
