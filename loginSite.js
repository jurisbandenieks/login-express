const path = require('path')

const express = require('express')
const app = express()

const cookieParser = require('cookie-parser')

const port = 5000

const helmet = require('helmet')
app.use(helmet())

app.use(express.static('public'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))

app.use((req, res, next) => {
  if (req.query.msg === 'fail') {
    res.locals.msg = `Sorry. This username or password doesn't exist`
  } else {
    res.locals.msg = ''
  }

  next()
})

app.get('/', (req, res, next) => {
  res.redirect('/login')
})

app.get('/login', (req, res, next) => {
  res.render('login')
})

app.post('/process_login', (req, res, next) => {
  const password = req.body.password
  const username = req.body.username

  if (password === 'x') {
    res.cookie('username', username)
    res.redirect('/welcome')
  } else {
    res.redirect('/login?msg=fail')
  }
})

app.get('/welcome', (req, res, next) => {
  res.render('welcome', {
    username: req.cookies.username
  })
})

app.get('/story/:story', (req, res, next) => {
  const story = req.params.story
  res.send(`<h1>Story: ${story}</h1>`)
})

app.get('/meme', (req, res, next) => {
  // this will render an image in browser
  // res.sendFile(path.join(__dirname, 'memes/js-protip.jpg'))

  res.download(path.join(__dirname, 'memes/js-protip.jpg'), 'jsmeme.jpg')
})

app.get('/logout', (req, res, next) => {
  res.clearCookie('username')
  res.redirect('/login')
})

app.listen(port)
